import React from 'react';
import { Card, CardBody, Col, Table } from 'reactstrap';
import { withTranslation } from 'react-i18next';

const ResponsiveTable = () => (
  <Col md={12} lg={12}>
    <Card>
      <CardBody>
        <Table responsive className="table--bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Link</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>About AIA</td>
              <td>
                <a href="https://vimeo.com/308812583">
                  https://vimeo.com/308812583
                </a>
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>Investment Portfolio</td>
              <td>
                <a href="https://drive.google.com/file/d/1A2xBOodqxdmKrUPfKbx2cr8IyHKusgsa/view?usp=drivesdk">
                  https://drive.google.com/file/d/1A2xBOodqxdmKrUPfKbx2cr8IyHKusgsa/view?usp=drivesdk
                </a>
              </td>
            </tr>
          </tbody>
        </Table>
      </CardBody>
    </Card>
  </Col>
);

export default withTranslation('common')(ResponsiveTable);
