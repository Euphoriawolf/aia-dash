import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import ResponsiveTable from './components/ResponsiveTable';
import withAuthorization from '../../utils/withAuthorization';

const Admin = () => (
  <Container>
    <Row>
      <Col md={12}>
        <h3 className="page-title">Files</h3>
        <h3 className="page-subhead subhead">A message from our founders</h3>
      </Col>
    </Row>
    <Row>
      <ResponsiveTable />
    </Row>
  </Container>
);

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(Admin);
