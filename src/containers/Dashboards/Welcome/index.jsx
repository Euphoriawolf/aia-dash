import React from 'react';
import {
  Col, Container, Row, Card, CardBody, Button,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const structure = [
  {
    title: 'Stocks',
    subheading: 'This is some sample text',
    link: '/stocks',
    buttonColor: 'primary',
    buttonText: 'Learn more',
  },
  {
    title: 'Crypto Assets',
    subheading: 'This is some sample text',
    link: '/crypto',
    buttonColor: 'primary',
    buttonText: 'Learn more',
  },
  {
    title: 'Knowledge Videos',
    subheading: 'This is some sample text',
    link: '/videos',
    buttonColor: 'warning',
    buttonText: 'Learn more',
  },
  {
    title: 'Files / Document',
    subheading: 'This is some sample text',
    link: 'files',
    buttonColor: 'warning',
    buttonText: 'Learn more',
  },
  {
    title: 'Deposit',
    subheading: 'This is some sample text',
    link: '/deposit',
    buttonColor: 'success',
    buttonText: 'Learn more',
  },
  {
    title: 'Withdraw',
    subheading: 'This is some sample text',
    link: '/withdraw',
    buttonColor: 'success',
    buttonText: 'Learn more',
  },
];

const WelcomeCard = ({
  title, subheading, link, buttonColor, buttonText,
}) => (
  <Col md={12} lg={6} xs={12}>
    <Card>
      <CardBody>
        <div className="card__title">
          <h5 className="bold-text">{title}</h5>
          <h5 className="subhead">{subheading}</h5>
        </div>
        <Button color={buttonColor} tag={Link} to={link}>
          {buttonText}
        </Button>
      </CardBody>
    </Card>
  </Col>
);

WelcomeCard.propTypes = {
  title: PropTypes.string.isRequired,
  subheading: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  buttonColor: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
};

const Welcome = () => (
  <Container className="dashboard">
    <Row>
      <Col md={12}>
        <h3 className="page-title">Welcome! </h3>
        <h3 className="page-subhead subhead">
          {' '}
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem
        </h3>
      </Col>
    </Row>
    <Row>
      {structure.map(config => (
        <Welcome
          title={config.title}
          subheading={config.subheading}
          link={config.link}
          buttonColor={config.buttonColor}
          buttonText={config.buttonText}
        />
      ))}
    </Row>
  </Container>
);

export default Welcome;
