import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import TradingViewWidget from 'react-tradingview-widget';
import Visits from './components/Visits';
import TotalPageViews from './components/TotalPageViews';
import NewUsers from './components/NewUsers';
import BounceRate from './components/BounceRate';
import withAuthorization from '../../../utils/withAuthorization';

// import ABTestingAnalytics from './components/ABTestingAnalytics';
// import SalesStatistic from './components/SalesStatistic';
// import VisitorsSessions from './components/VisitorsSessions';
// import BounceRateArea from './components/BounceRateArea';
// import AudienceByCountry from './components/AudienceByCountry';
// import BudgetStatistic from './components/BudgetStatistic';
// import BestSellingRegions from './components/BestSellingRegions';
// import GoalsCompletion from './components/GoalsCompletion';

const DefaultDashboard = () => (
  <Container className="dashboard">
    <Row>
      <Col md={12}>
        <h3 className="page-title">Stocks</h3>
      </Col>
    </Row>
    <Row>
      <Visits />
      <TotalPageViews />
      <NewUsers />
      <BounceRate />
    </Row>
    <Row className="pt-1" style={{ height: '50vh' }}>
      <TradingViewWidget symbol="NASDAQ:AAPL" autosize />
    </Row>
    {/* <Row>
      <ABTestingAnalytics />
      <BounceRateArea />
      <VisitorsSessions />
      <SalesStatistic />
      <BudgetStatistic />
      <AudienceByCountry />
      <BestSellingRegions />
      <GoalsCompletion />
    </Row> */}
  </Container>
);

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(DefaultDashboard);
