import React, { PureComponent } from 'react';
import { Col, Container, Row } from 'reactstrap';
// import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
// import PropTypes from 'prop-types';
import BTC from './components/BTC';
import ETH from './components/ETH';
import BCH from './components/BCH';
import XRP from './components/XRP';
import TradeHistory from './components/TradeHistory';
import BtcEth from './components/BtcEth';
import withAuthorization from '../../../utils/withAuthorization';
// import CryptotrendsToday from './components/CryptotrendsToday';
// import TopTen from './components/TopTen';
// import PlaceOrder from './components/PlaceOrder';
// import { deleteCryptoTableData } from '../../../redux/actions/cryptoTableActions';
// import { CryptoTableProps } from '../../../shared/prop-types/TablesProps';

class CryptoDashboard extends PureComponent {
  // onDeleteCryptoTableData = (index, e) => {
  //   const { dispatch, cryptoTable } = this.props;
  //   e.preventDefault();
  //   const arrayCopy = [...cryptoTable];
  //   arrayCopy.splice(index, 1);
  //   dispatch(deleteCryptoTableData(arrayCopy));
  // };

  render() {
    return (
      <Container className="dashboard">
        <Row>
          <Col md={12}>
            <h3 className="page-title">Crypto Assets</h3>
          </Col>
        </Row>
        <Row>
          <BTC />
          <ETH />
          <BCH />
          <XRP />
        </Row>
        <Row>
          <TradeHistory />
          <BtcEth />
          {/* <CryptotrendsToday /> */}
          {/* <PlaceOrder /> */}
          {/* <TopTen cryptoTable={cryptoTable} onDeleteCryptoTableData={this.onDeleteCryptoTableData} /> */}
        </Row>
      </Container>
    );
  }
}
const authCondition = authUser => !!authUser;

export default connect(state => ({
  cryptoTable: state.cryptoTable.items
}))(withAuthorization(authCondition)(CryptoDashboard));
