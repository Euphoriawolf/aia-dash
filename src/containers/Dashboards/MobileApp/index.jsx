import React from 'react';
import { Col, Container, Row } from 'reactstrap';

import SessionShort from './components/SessionShort';
import ActiveUsersShort from './components/ActiveUsersShort';
import NewUsersShort from './components/NewUsersShort';
import PageViewsShort from './components/PageViewsShort';
import BasicTable from '../../Tables/BasicTables/components/BasicTable';
import TradingViewWidget from 'react-tradingview-widget';
import Commissions from './components/Commissions';
import { db } from '../../../utils/firebase/firebase';
import withAuthorization from '../../../utils/withAuthorization';

const sizes = {
  md: 4,
  lg: 4,
  xs: 12,
  xl: 4
};

class MobileAppDashboard extends React.Component {
  state = {
    id: '',
    account: {}
  };

  componentDidMount() {
    const localId = localStorage.getItem('uid');
    const userRef = db.collection('Users').doc(`${localId}`);

    userRef.get().then(doc => {
      if (doc.exists) {
        this.setState({ account: doc.data().account });
      } else {
        console.log('no such document');
      }
    });
  }
  render() {
    const { account } = this.state;

    return (
      <Container className="dashboard">
        <Row>
          <Col md={12}>
            <h3 className="page-title">Overview </h3>
          </Col>
        </Row>
        <Row>
          <SessionShort balance={account.balance} />
          <ActiveUsersShort profit={account.profit} />
          <NewUsersShort deposit={account.lastDeposit} />
          <PageViewsShort withdrawal={account.lastWithdrawal} />
          <Commissions commissions={account.commissions} />
        </Row>
        <Row>
          <BasicTable sizes={sizes} />
          <Col md={4} xl={3} lg={8} xs={4}>
            <TradingViewWidget symbol="COINBASE:BTCUSD" autosize />
          </Col>
        </Row>
      </Container>
    );
  }
}

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(MobileAppDashboard);
