/* eslint-disable react/no-array-index-key */
import React from 'react';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import TrendingUpIcon from 'mdi-react/TrendingUpIcon';
import { Card, CardBody, Col } from 'reactstrap';

const ActiveUsersShort = ({ t, profit }) => (
  <Col md={12} xl={3} lg={6} xs={12}>
    <Card>
      <CardBody className="dashboard__card-widget">
        <div className="mobile-app-widget">
          <div className="mobile-app-widget__top-line mobile-app-widget__top-line--blue">
            <p className="mobile-app-widget__total-stat">($) {profit} </p>
            <TrendingUpIcon className="dashboard__trend-icon" />
          </div>
          <div className="mobile-app-widget__title mt-4">
            <h5>Profit</h5>
          </div>
          {/* <div className="progress-wrap progress-wrap--small progress-wrap--blue-gradient progress-wrap--label-top">
            <Progress value={32}>
              <p className="progress__label">32%</p>
            </Progress>
          </div> */}
        </div>
      </CardBody>
    </Card>
  </Col>
);

ActiveUsersShort.propTypes = {
  t: PropTypes.func.isRequired
};

export default withTranslation('common')(ActiveUsersShort);
