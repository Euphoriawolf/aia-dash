import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import MatTable from './components/MatTable';
import withAuthorization from '../../../utils/withAuthorization';

const MaterialTable = () => (
  <Container>
    <Row>
      <Col md={12}>
        <h3 className="page-title">Videos</h3>
      </Col>
    </Row>
    <Row>
      <MatTable />
    </Row>
  </Container>
);

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(MaterialTable);
