import React from 'react';
import { Card, CardBody, Col, Table } from 'reactstrap';
import { withTranslation } from 'react-i18next';
import { getTransactions } from '../../../../utils/firebase/db';

class TableWrapper extends React.Component {
  state = {
    transactions: []
  };
  componentDidMount() {
    getTransactions().then(val => this.setState({ transactions: val }));
  }
  render() {
    const { sizes } = this.props;
    const { transactions } = this.state;
    return <BasicTable sizes={sizes} tz={transactions} />;
  }
}

const BasicTable = ({ sizes, tz }) => (
  <Col md={sizes.md} lg={sizes.lg} xl={sizes.xl}>
    <Card>
      <CardBody>
        <div className="card__title">
          <h5 className="bold-text">Transaction History</h5>
          {/* <h5 className="subhead">Use default table</h5> */}
        </div>
        <Table responsive hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Type</th>
              <th>Amount</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            {tz !== undefined ? (
              tz.map((t, i) => (
                <tr key={i}>
                  <td>{i + 1}</td>
                  <td>{t.type}</td>
                  <td>$ {t.amount}</td>
                  <td>{t.date}</td>
                </tr>
              ))
            ) : (
              <tr>
                <td>Nil</td>
                <td>Nil</td>
                <td>Nil</td>
                <td>Nil</td>
              </tr>
            )}
          </tbody>
        </Table>
      </CardBody>
    </Card>
  </Col>
);

export default withTranslation('common')(TableWrapper);
