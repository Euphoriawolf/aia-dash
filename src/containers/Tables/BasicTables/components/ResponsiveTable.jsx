import React from 'react';
import {
  Card, CardBody, Col, Table,
} from 'reactstrap';
import { withTranslation } from 'react-i18next';

const ResponsiveTable = () => (
  <Col md={12} lg={12}>
    <Card>
      <CardBody>
        {/* <div className="card__title">
          <h5 className="bold-text">{t('tables.basic_tables.responsive_table')}</h5>
          <h5 className="subhead">Use default table with property <span className="red-text">responsive</span></h5>
        </div> */}
        <Table responsive className="table--bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Link</th>
              {/* <th>Username</th>
              <th>Age</th>
              <th>Date</th>
              <th>Location</th>
              <th>Status</th> */}
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>About AIA</td>
              <td><a href="https://vimeo.com/308812583">https://vimeo.com/308812583</a></td>
              {/* <td>@dragon</td>
              <td>21</td>
              <td>1990/12/01</td>
              <td>Melbourne</td>
              <td><Badge color="success">In Progress</Badge></td> */}
            </tr>
            <tr>
              <td>2</td>
              <td>Investment Portfolio</td>
              <td>
                <a href="https://drive.google.com/file/d/1A2xBOodqxdmKrUPfKbx2cr8IyHKusgsa/view?usp=drivesdk">
                https://drive.google.com/file/d/1A2xBOodqxdmKrUPfKbx2cr8IyHKusgsa/view?usp=drivesdk
                </a>
              </td>
              {/* <td>@boboby</td>
              <td>35</td>
              <td>1992/12/01</td>
              <td>Tokio</td>
              <td><Badge color="primary">Completed</Badge></td> */}
            </tr>
          </tbody>
        </Table>
      </CardBody>
    </Card>
  </Col>
);


export default withTranslation('common')(ResponsiveTable);
