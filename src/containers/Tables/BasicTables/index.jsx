import React from 'react';
import { Col, Container, Row } from 'reactstrap';
// import { withTranslation } from 'react-i18next';
// import PropTypes from 'prop-types';
// import BasicTable from './components/BasicTable';
// import BorderedTable from './components/BorderedTable';
// import HeadAccentTable from './components/HeadAccentTable';
// import ColoredStringTable from './components/ColoredStringTable';
import ResponsiveTable from './components/ResponsiveTable';
import withAuthorization from '../../../utils/withAuthorization';

const BasicTables = () => (
  <Container>
    <Row>
      <Col md={12}>
        <h3 className="page-title">Files</h3>
        <h3 className="page-subhead subhead">A message from our founders</h3>
      </Col>
    </Row>
    <Row>
      {/* <BasicTable /> */}
      {/* <BorderedTable /> */}
      {/* <HeadAccentTable /> */}
      {/* <ColoredStringTable /> */}
      <ResponsiveTable />
    </Row>
  </Container>
);

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(BasicTables);
