import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import ResponsiveTable from './components/ResponsiveTable';
import withAuthorization from '../../../utils/withAuthorization';
import { db } from '../../../utils/firebase/firebase';

class AdminWrapper extends React.Component {
  state = {
    users: []
  };

  componentDidMount() {
    db.collection('Users')
      .get()
      .then(snapshot => {
        let users = [];

        snapshot.forEach(doc => {
          let data = { id: doc.id, details: doc.data() };
          users.push(data);
        });
        // localStorage.setItem('allUsers', users);
        this.setState({ users });
        // console.log(users);
      });
    // getUsers();
    // const users = localStorage.getItem('allUsers');
    // console.log(users);
    // this.setState({ users: getUsers });
  }

  render() {
    const { users } = this.state;
    console.log(users);
    return <Admin users={users} />;
  }
}

const Admin = ({ users }) => (
  <Container>
    <Row>
      <Col md={12}>
        <h3 className="page-title">Users</h3>
        <h3 className="page-subhead subhead">
          A list of everyone on the platform and their details
        </h3>
      </Col>
    </Row>
    <Row>
      <ResponsiveTable users={users} />
    </Row>
  </Container>
);

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(AdminWrapper);
