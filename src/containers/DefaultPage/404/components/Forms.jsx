import React from 'react';
import { Button, Table } from 'reactstrap';
import { db } from '../../../../utils/firebase/firebase';
import firebase from 'firebase';
import { BasicNotification } from '../../../../shared/components/Notification';
import NotificationSystem from 'rc-notification';

let notification = null;

const showNotification = () => {
  notification.notice({
    content: (
      <BasicNotification title="👋 Added" message="refresh to confirm" />
    ),
    duration: 5,
    closable: true,
    style: { top: 0, left: 'calc(100vw - 100%)' },
    className: 'right-up'
  });
};

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value
});

const accountInitialState = {
  balance: '',
  lastDeposit: '',
  lastWithdrawal: '',
  profit: '',
  commissions: ''
};
class AccountForm extends React.Component {
  state = accountInitialState;

  handleSubmit = e => {
    e.preventDefault();
    const {
      balance,
      lastDeposit,
      lastWithdrawal,
      profit,
      commissions
    } = this.state;
    const { account } = this.props;
    const id = this.props.id;
    const userRef = db.collection('Users').doc(id);

    userRef
      .update({
        account: {
          balance: balance !== '' ? balance : account.balance,
          lastWithdrawal:
            lastWithdrawal !== '' ? lastWithdrawal : account.lastWithdrawal,
          lastDeposit: lastDeposit !== '' ? lastDeposit : account.lastDeposit,
          profit: profit !== '' ? profit : account.profit,
          commissions: commissions !== '' ? commissions : account.commissions
        }
      })
      .then(() => {
        NotificationSystem.newInstance({}, n => (notification = n));
        setTimeout(() => showNotification(), 700);
        this.setState({ ...accountInitialState });
      });
  };
  render() {
    const {
      balance,
      lastDeposit,
      lastWithdrawal,
      profit,
      commissions
    } = this.state;
    const { account } = this.props;
    const { handleSubmit } = this;
    return (
      <form action="" className="form form--horizontal" onSubmit={handleSubmit}>
        <span className="form__form-group-label">Balance</span>
        <div className="form__form-group-field">
          <input
            value={balance}
            onChange={event =>
              this.setState(byPropKey('balance', event.target.value))
            }
            type="text"
            placeholder={account.balance}
          />
        </div>
        <span className="form__form-group-label">last Deposit</span>
        <div className="form__form-group-field">
          <input
            value={lastDeposit}
            onChange={event =>
              this.setState(byPropKey('lastDeposit', event.target.value))
            }
            type="text"
            placeholder={account.lastDeposit}
          />
        </div>
        <span className="form__form-group-label">lastWithdrawal</span>
        <div className="form__form-group-field">
          <input
            value={lastWithdrawal}
            onChange={event =>
              this.setState(byPropKey('lastWithdrawal', event.target.value))
            }
            type="text"
            placeholder={account.lastWithdrawal}
          />
        </div>
        <span className="form__form-group-label">profit</span>
        <div className="form__form-group-field">
          <input
            value={profit}
            onChange={event =>
              this.setState(byPropKey('profit', event.target.value))
            }
            type="text"
            placeholder={account.profit}
          />
        </div>
        <span className="form__form-group-label">commissions</span>
        <div className="form__form-group-field">
          <input
            value={commissions}
            onChange={event =>
              this.setState(byPropKey('commissions', event.target.value))
            }
            type="text"
            placeholder={account.commissions}
          />
        </div>
        <Button color="primary" type="submit">
          Update
        </Button>
      </form>
    );
  }
}
const initialState = {
  amount: '',
  date: '',
  type: ''
};

class TransactionForm extends React.Component {
  state = initialState;
  handleSubmit = e => {
    e.preventDefault();
    this.setState({ ...initialState });
    const newTz = this.state;
    const id = this.props.id;
    const userRef = db.collection('Users').doc(id);

    userRef.update({
      transactions: firebase.firestore.FieldValue.arrayUnion(newTz)
    });

    NotificationSystem.newInstance({}, n => (notification = n));
    setTimeout(() => showNotification(), 700);
    //push this newtz to firebase
  };

  handleDelete = t => {
    const id = this.props.id;
    const userRef = db.collection('Users').doc(id);

    userRef.update({
      transactions: firebase.firestore.FieldValue.arrayRemove(t)
    });

    NotificationSystem.newInstance({}, n => (notification = n));
    setTimeout(() => showNotification(), 700);
  }

  render() {
    const { amount, type, date } = this.state;
    const { handleSubmit, handleDelete } = this;
    const { tz } = this.props;
    return (
      <div>
        <h3>Transactions</h3>
        <form
          action=""
          className="form form--horizontal"
          onSubmit={handleSubmit}
        >
          <span className="form__form-group-label">amount</span>
          <div className="form__form-group-field">
            <input
              value={amount}
              onChange={event =>
                this.setState(byPropKey('amount', event.target.value))
              }
              type="text"
              placeholder=""
            />
          </div>
          <span className="form__form-group-label">date</span>
          <div className="form__form-group-field">
            <input
              value={date}
              onChange={event =>
                this.setState(byPropKey('date', event.target.value))
              }
              type="text"
              placeholder=""
            />
          </div>
          <span className="form__form-group-label">type</span>
          <div className="form__form-group-field">
            <input
              value={type}
              onChange={event =>
                this.setState(byPropKey('type', event.target.value))
              }
              type="text"
              placeholder=""
            />
          </div>
          <Button color="primary" type="submit">
            Add
          </Button>
        </form>
        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>Type</th>
              <th>Amount</th>
              <th>Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {tz !== undefined ? (
              tz.map((t, i) => (
                <tr key={i}>
                  <td>{i + 1}</td>
                  <td>{t.type}</td>
                  <td>$ {t.amount}</td>
                  <td>{t.date}</td>
                  <td><Button color="danger" onClick={()=> handleDelete(t)}>Delete</Button></td>
                </tr>
              ))
            ) : (
              <tr>
                <td>Nil</td>
                <td>Nil</td>
                <td>Nil</td>
                <td>Nil</td>
              </tr>
            )}
          </tbody>
        </Table>
      </div>
    );
  }
}

export { AccountForm, TransactionForm };
