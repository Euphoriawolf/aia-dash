import React from 'react';
import { Card, CardBody, Col, Table } from 'reactstrap';
import { withTranslation } from 'react-i18next';
import UserModal from './UserModal';

const ResponsiveTable = ({ users }) => (
  <Col md={12} lg={12}>
    <Card>
      <CardBody>
        <Table responsive className="table--bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user, i) => (
              <tr key={i}>
                <td>{i + 1}</td>
                <td>
                  {user.details.username}
                  <UserModal
                    btn="edit"
                    color="primary"
                    title="Edit User"
                    message={user.id}
                    user={user}
                  />
                </td>
                <td>{user.details.email}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </CardBody>
    </Card>
  </Col>
);

export default withTranslation('common')(ResponsiveTable);
