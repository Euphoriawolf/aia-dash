import React, { PureComponent } from 'react';
// import { Field, reduxForm } from 'redux-form';
import KeyVariantIcon from 'mdi-react/KeyVariantIcon';
import AccountOutlineIcon from 'mdi-react/AccountOutlineIcon';
import { Link, withRouter } from 'react-router-dom';
// import PropTypes from 'prop-types';
// import renderCheckBoxField from '../../../../shared/components/form/CheckBox';
import { auth } from '../../../../utils/firebase';

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value
});

const initialState = {
  email: '',
  password: '',
  error: null,
  showPassword: false
};

class LogInForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { ...initialState };
    this.showPassword = this.showPassword.bind(this);
  }

  onSubmit = event => {
    const { email, password } = this.state;
    const { history } = this.props;
    auth
      .doSignInWithEmailAndPassword(email, password)
      .then(authUser => {
        this.setState({ ...initialState });
        const id = authUser.user.uid;
        const email = authUser.user.email;
        localStorage.setItem('uid', id);
        localStorage.setItem('email', email);
        history.push('/home');
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });
    event.preventDefault();
  };

  showPassword(e) {
    e.preventDefault();
    this.setState(prevState => ({ showPassword: !prevState.showPassword }));
  }

  render() {
    const { showPassword, email, password } = this.state;

    return (
      <form className="form" onSubmit={this.onSubmit}>
        <div className="form__form-group">
          <span className="form__form-group-label">Username</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <AccountOutlineIcon />
            </div>
            <input
              value={email}
              onChange={event =>
                this.setState(byPropKey('email', event.target.value))
              }
              type="text"
              placeholder="email"
            />
          </div>
        </div>
        <div className="form__form-group">
          <span className="form__form-group-label">Password</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <KeyVariantIcon />
            </div>
            <input
              value={password}
              onChange={event =>
                this.setState(byPropKey('password', event.target.value))
              }
              type="password"
              placeholder="password"
            />
            <button
              type="button"
              className={`form__form-group-button${
                showPassword ? ' active' : ''
              }`}
              onClick={e => this.showPassword(e)}
            >
              {/* <EyeIcon /> */}
            </button>
          </div>
          <div className="account__forgot-password">
            <a href="/easydev/log_in">Forgot password?</a>
          </div>
        </div>
        {/* <div className="form__form-group">
          <div className="form__form-group-field">
            <input
              name="remember_me"
              component={renderCheckBoxField}
              label="Remember me"
            />
          </div>
        </div> */}
        <div className="account__btns">
          <button className="btn btn-primary account__btn">Sign In</button>

          <Link className="btn btn-outline-primary account__btn" to="/register">
            Create Account
          </Link>
        </div>
      </form>
    );
  }
}

export default withRouter(LogInForm);
