import React, { PureComponent } from 'react';
import {
  Col,
  Container,
  Row,
  Card,
  CardBody,
  Button,
  ButtonToolbar
} from 'reactstrap';
import { withTranslation } from 'react-i18next';
import { BasicNotification } from '../../../shared/components/Notification';
import NotificationSystem from 'rc-notification';
import { db } from '../../../utils/firebase';
import ProfileMain from '../Profile/components/ProfileMain';

let notification = null;

const showNotification = () => {
  notification.notice({
    content: (
      <BasicNotification
        title="Referral code received"
        message="Your account will be updated on confirmation"
      />
    ),
    duration: 5,
    closable: true,
    style: { top: 0, left: 'calc(100vw - 100%)' },
    className: 'right-up'
  });
};

const profileSizes = {
  md: 4,
  lg: 4,
  xl: 4,
  xs: 12
};

class Referral extends PureComponent {
  state = {
    code: ''
  };

  handleChange = e => {
    this.setState({ code: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ code: '' });
    NotificationSystem.newInstance({}, n => (notification = n));
    setTimeout(() => showNotification(), 700);
    db.getRCode(this.state.code);
    //trigger a notication here
  };

  render() {
    const { handleSubmit, handleChange } = this;
    const { code } = this.state;
    return (
      <Container>
        <Row>
          <Col md={12}>
            <h3 className="page-title">Referral</h3>
            <h3 className="page-subhead subhead">
              Increase returns for you and a referrer
            </h3>
          </Col>
        </Row>
        <Row>
          <Col md={8} lg={8}>
            <Card>
              <CardBody>
                <form className="form form--horizontal" onSubmit={handleSubmit}>
                  <span className="form__form-group-label">Code</span>

                  <div className="form__form-group-field">
                    <input
                      value={code}
                      type="text"
                      placeholder="enter referral code"
                      onChange={handleChange}
                    />
                  </div>
                  <ButtonToolbar className="form__button-toolbar">
                    <Button color="primary" type="submit">
                      Submit
                    </Button>
                    <Button
                      type="button"
                      onClick={() => this.setState({ code: '' })}
                    >
                      Cancel
                    </Button>
                  </ButtonToolbar>
                </form>
              </CardBody>
            </Card>
          </Col>
          <ProfileMain sizes={profileSizes} />
        </Row>
      </Container>
    );
  }
}

export default withTranslation('common')(Referral);
