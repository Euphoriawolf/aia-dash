import React, { Component } from 'react';
import { Card, CardBody, Col, Button } from 'reactstrap';
import { db } from '../../../../utils/firebase/firebase';
// import MessageTextOutlineIcon from 'mdi-react/MessageTextOutlineIcon';
import CustomUploadButton from 'react-firebase-file-uploader/lib/CustomUploadButton';
import firebase from 'firebase';

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value
});

// const Ava = `${process.env.PUBLIC_URL}/img/12.png`;

class ProfilePage extends Component {
  state = {
    data: {},
    account: {},
    avatar: '',
    isUploading: false,
    progress: 0,
    avatarURL: '',
    fullName: '',
    phoneNumber: '',
    country: '',
    postalCode: '',
    profession:'',
    document: '',
    documentURL: ''
  };

  handleUploadStart = () => this.setState({ isUploading: true, progress: 0 });
  handleProgress = progress => this.setState({ progress });
  handleUploadError = error => {
    this.setState({ isUploading: false });
    console.error(error);
  };
  handleUploadSuccess = filename => {
    this.setState({ avatar: filename, progress: 100, isUploading: false });
    firebase
      .storage()
      .ref('images')
      .child(filename)
      .getDownloadURL()
      .then(url => {
        const id = localStorage.getItem('uid');
        const userRef = db.collection('Users').doc(id);

        userRef.update({
          avatarURL: url
        });
      });

    alert('upload successful, refresh the page');
  };

  handleDocumentUpload = filename => {
    this.setState({ document: filename, progress: 100, isUploading: false})
    firebase
      .storage()
      .ref('images')
      .child(filename)
      .getDownloadURL()
      .then( url => {
        const id = localStorage.getItem('uid')
        const userRef = db.collection('Users').doc(id)

        userRef.update({
          documentURL: url
        })
      })
  }

  handleFormSubmit = e => {
    e.preventDefault();
    const { phoneNumber, fullName, country,postalCode,profession, data } = this.state;
    const id = localStorage.getItem('uid');
    const userRef = db.collection('Users').doc(id);

    userRef.update({
      phoneNumber: phoneNumber !== '' ? phoneNumber : data.phoneNumber,
      country: country !== '' ? country : data.country,
      username: fullName !== '' ? fullName : data.username,
      postalCode: postalCode !== '' ? postalCode : data.postalCode,
      profession: profession !== '' ? profession : data.profession,
    });
    this.setState({ phoneNumber: '', country: '', fullName: '', profession:'', postalCode: ''});
    alert('Details updated succefully, refresh the page');
    // window.location.reload();
  };

  componentDidMount() {
    const localId = localStorage.getItem('uid');
    const userRef = db.collection('Users').doc(`${localId}`);

    userRef.get().then(doc => {
      if (doc.exists) {
        this.setState({ data: doc.data() });
        this.setState({ account: doc.data().account });
      } else {
        console.log('no such document');
      }
    });
  }
  render() {
    const {
      data,
      account,
      isUploading,
      fullName,
      country,
      postalCode,
      profession,
      phoneNumber
    } = this.state;
    const { sizes } = this.props;
    const {
      handleUploadError,
      handleProgress,
      handleUploadStart,
      handleUploadSuccess,
      handleDocumentUpload,
      handleFormSubmit
    } = this;
    return (
      <Col md={sizes.md} lg={sizes.lg} xl={sizes.xl} xs={sizes.xs}>
        <Card>
          <CardBody className="profile__card">
            <div className="profile__information row">
              <form action="" className="col">
                <div className="profile__avatar">
                  {isUploading && <p>Loading</p>}
                  {/* {avatarURL && <img src={data.avatarURL} alt="avatar" />} */}

                  <img src={data.avatarURL} alt="avatar" />
                </div>
                
                <CustomUploadButton
                  accept="image/*"
                  name="avatar"
                  randomizeFilename
                  storageRef={firebase.storage().ref('images')}
                  onUploadStart={handleUploadStart}
                  onUploadError={handleUploadError}
                  onUploadSuccess={handleUploadSuccess}
                  onProgress={handleProgress}
                  style={{
                    backgroundColor: 'steelblue',
                    color: 'white',
                    padding: 10,
                    borderRadius: 4,
                    marginTop: 10
                  }}
                >
                  Select your avatar
                </CustomUploadButton>
              </form>
              <form action="" className="col">
                <div className="profile__avatar">
                  {isUploading && <p>Loading</p>}
                  {/* {avatarURL && <img src={data.avatarURL} alt="avatar" />} */}

                  <img src={data.documentURL} alt="document" />
                </div>
                
                <CustomUploadButton
                  accept="image/*"
                  name="avatar"
                  randomizeFilename
                  storageRef={firebase.storage().ref('images')}
                  onUploadStart={handleUploadStart}
                  onUploadError={handleUploadError}
                  onUploadSuccess={handleDocumentUpload}
                  onProgress={handleProgress}
                  style={{
                    backgroundColor: 'steelblue',
                    color: 'white',
                    padding: 10,
                    borderRadius: 4,
                    marginTop: 10
                  }}
                >
                  Upload your Document(Driver's License / ID card / Utility bill)
                </CustomUploadButton>
              </form>
              
              <form
                action=""
                onSubmit={handleFormSubmit}
                className="form form-horizontal col"
              >
                {/* <div>
                  <h3 className="profile__name">Update Profile</h3>
                  <br/>
                </div> */}

                <span className="form__form-group-label">Full Name</span>
                <div className="form__form-group-field">
                  <input
                    value={fullName}
                    onChange={event =>
                      this.setState(byPropKey('fullName', event.target.value))
                    }
                    type="text"
                    placeholder={data.username}
                  />
                </div>
                <span className="form__form-group-label">Country</span>
                <div className="form__form-group-field">
                  <input
                    value={country}
                    onChange={event =>
                      this.setState(byPropKey('country', event.target.value))
                    }
                    type="text"
                    placeholder={data.country}
                  />
                </div>
                <span className="form__form-group-label">Phone Number</span>
                <div className="form__form-group-field">
                  <input
                    value={phoneNumber}
                    onChange={event =>
                      this.setState(
                        byPropKey('phoneNumber', event.target.value)
                      )
                    }
                    type="text"
                    placeholder={data.phoneNumber}
                  />
                </div>
                <span className="form__form-group-label">Postal Code</span>
                <div className="form__form-group-field">
                  <input
                    value={postalCode}
                    onChange={event =>
                      this.setState(
                        byPropKey('postalCode', event.target.value)
                      )
                    }
                    type="text"
                    placeholder={data.postalCode}
                  />
                </div>
                <span className="form__form-group-label">Profession</span>
                <div className="form__form-group-field">
                  <input
                    value={profession}
                    onChange={event =>
                      this.setState(
                        byPropKey('profession', event.target.value)
                      )
                    }
                    type="text"
                    placeholder={data.profession}
                  />
                </div>
                <Button color="primary" type="submit" className="mt-2">
                  Update
                </Button>
              </form>
              <div className="profile__data">
                <p className="profile__name">{data.username}</p>
                {/* <p className="profile__work">Gold Plan</p> */}
                <p className="profile__contact">{data.email}</p>
              </div>
            </div>
            <div className="profile__stats">
              <div className="profile__stat">
                <p className="profile__stat-number">${account.balance}</p>
                <p className="profile__stat-title">Balance</p>
              </div>
              <div className="profile__stat">
                <p className="profile__stat-number">${account.profit}</p>
                <p className="profile__stat-title">Profit</p>
              </div>
            </div>
          </CardBody>
        </Card>
      </Col>
    );
  }
}

export default ProfilePage;
