import React, { Component } from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import { db } from '../../../../utils/firebase/firebase';
// import MessageTextOutlineIcon from 'mdi-react/MessageTextOutlineIcon';

const Ava = `${process.env.PUBLIC_URL}/img/12.png`;

class ProfileMain extends Component {
  state = {
    data: {},
    account: {}
  };

  componentDidMount() {
    const localId = localStorage.getItem('uid');
    const userRef = db.collection('Users').doc(`${localId}`);

    userRef.get().then(doc => {
      if (doc.exists) {
        this.setState({ data: doc.data() });
        this.setState({ account: doc.data().account });
      } else {
        console.log('no such document');
      }
    });
  }
  render() {
    const { data, account } = this.state;
    const { sizes } = this.props;
    return <Prof sizes={sizes} data={data} account={account} />;
  }
}

const Prof = ({ sizes, data, account }) => (
  <Col md={sizes.md} lg={sizes.lg} xl={sizes.xl} xs={sizes.xs}>
    <Card>
      <CardBody className="profile__card">
        <div className="profile__information">
          <div className="profile__avatar">
            <img src={Ava} alt="avatar" />
          </div>
          <div className="profile__data">
            <p className="profile__name">{data.username}</p>
            {/* <p className="profile__work">Gold Plan</p> */}
            <p className="profile__contact">{data.email}</p>
          </div>
        </div>
        <div className="profile__stats">
          <div className="profile__stat">
            <p className="profile__stat-number">${account.balance}</p>
            <p className="profile__stat-title">Balance</p>
          </div>
          <div className="profile__stat">
            <p className="profile__stat-number">${account.profit}</p>
            <p className="profile__stat-title">Profit</p>
          </div>
        </div>
      </CardBody>
    </Card>
  </Col>
);

export default ProfileMain;
