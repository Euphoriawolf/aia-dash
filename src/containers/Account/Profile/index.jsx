import React from 'react';
import { Col, Container, Row, CardBody, Button, CardTitle } from 'reactstrap';
import ProfilePage from './components/ProfilePage';
import NotificationSystem from 'rc-notification';
import { BasicNotification } from '../../../shared/components/Notification';
import { db } from '../../../utils/firebase/firebase';
import { doPasswordReset } from '../../../utils/firebase/auth';

let notification = null;

const showNotification = () => {
  notification.notice({
    content: (
      <BasicNotification
        title="👋 Reset Password request received"
        message="check your email on steps to reset your password"
      />
    ),
    duration: 5,
    closable: true,
    style: { top: 0, left: 'calc(100vw - 100%)' },
    className: 'right-up'
  });
};

const profileSizes = {
  md: 8,
  lg: 8,
  xl: 8,
  xs: 12
};

const ResetPassword = () => {
  const handleSubmit = () => {
    NotificationSystem.newInstance({}, n => (notification = n));
    setTimeout(() => showNotification(), 700);
    sendEmail();
  };

  const sendEmail = () => {
    const localId = localStorage.getItem('uid');
    const userRef = db.collection('Users').doc(`${localId}`);

    userRef.get().then(doc => {
      if (doc.exists) {
        doPasswordReset(doc.data().email);
      } else {
        console.log('no such document');
      }
    });
  };

  return (
    <Col md={4} lg={4}>
      <CardBody>
        <CardTitle>
          <h3 className="subhead">Reset Password</h3>
        </CardTitle>
        <div>
          {/* <p className="profile__name">Reset Password</p> */}
          <h6 className="mb-4">
            An email will be sent to you with a link to change your password
          </h6>
        </div>
        <Button color="primary" type="submit" onClick={handleSubmit}>
          reset
        </Button>
      </CardBody>
    </Col>
  );
};

const Profile = () => {
  return (
    <Container>
      <div className="profile">
        <Row>
          <ProfilePage sizes={profileSizes} />
          <ResetPassword />
        </Row>
      </div>
    </Container>
  );
};

export default Profile;
