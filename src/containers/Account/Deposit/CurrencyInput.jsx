import React from 'react';
import { Card, CardBody, CardTitle } from 'reactstrap';
import ModalComponent from '../../../shared/components/Modal';

const Message = ({ title, amount, address }) => (
  <div>
    <h3>
      You can send {amount.toFixed(5)} {title} to this address:
    </h3>
    <br />
    <h4>{address}</h4>
  </div>
);

const CurrencyInput = ({
  title,
  handleChange,
  handleSubmit,
  amount,
  amountInCrypto,
  address
}) => {
  return (
    <Card>
      {/* <img src={image} alt="currency" width="100px" height="100px" /> */}
      <CardBody>
        <CardTitle>
          <h3 className="mb-4"> {title}</h3>
          <form className="form form--horizontal" onSubmit={handleSubmit}>
            <span className="form__form-group-label">Amount($)</span>

            <div className="form__form-group-field mb-4">
              <input
                value={amount}
                type="number"
                placeholder="enter amount"
                onChange={handleChange}
              />
            </div>
          </form>
          {/* <Button> */}
          <ModalComponent
            color="primary"
            title={`Deposit via ${title}`}
            btn="Select"
            open={true}
            header
            amount={amount}
            // message={`You can send 0.63 ${title} to this address: 16t6to6oTx88UkdnGt9K48v4eYXJyjrv8m85`}
            message={
              <Message
                title={title}
                amount={amountInCrypto}
                address={address}
              />
            }
          />
          {/* </Button> */}
        </CardTitle>
      </CardBody>
    </Card>
  );
};

export default CurrencyInput;
