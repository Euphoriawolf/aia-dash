import React, { PureComponent } from 'react';
import { Col, Container, Row } from 'reactstrap';
import { withTranslation } from 'react-i18next';
import { BasicNotification } from '../../../shared/components/Notification';
import NotificationSystem from 'rc-notification';
// import ProfileMain from '../Profile/components/ProfileMain';
import CurrencyInput from './CurrencyInput';

let notification = null;

const showNotification = () => {
  notification.notice({
    content: (
      <BasicNotification
        title="👋 Deposit Request received"
        message="You wil be contacted via email on the deposit process"
      />
    ),
    duration: 5,
    closable: true,
    style: { top: 0, left: 'calc(100vw - 100%)' },
    className: 'right-up'
  });
};

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value
});

const currencies = [
  {
    title: 'Bitcoin',
    id: 'btc',
    address: '1Db2unyCJWPMeRVUm7sFMyf2j5mYfNdnfd'
  },
  {
    title: 'Ethereum',
    id: 'eth',
    address: '0x8D56d18ab3f4e19c7C88d42c0bF79684D6d9604e'
  },
  {
    title: 'Bitcoin Cash',
    id: 'bch',
    address: 'qp7r2gy5whrn5r39ra3ccv7dy80cvkvgfugyuw0jrf'
  },
  {
    title: 'Litecoin',
    id: 'ltc',
    address: 'LdQ9t7E67QhZSsUTBauKWy7ujaTgMgRxkg'
  },
  {
    title: 'Ripple',
    id: 'xrp',
    address: 'rEb8TK3gBgk5auZkwc6sHnwrGVJH8DuaLh     Tag:107695192'
  }
];

class Deposit extends PureComponent {
  state = {
    amount: '',
    btc: '',
    eth: '',
    bch: '',
    ltc: '',
    xrp: '',
    btcUSD: '',
    ethUSD: '',
    bchUSD: '',
    ltcUSD: '',
    xrpUSD: ''
  };

  componentDidMount() {
    currencies.map(c => {
      //get the usd price for id
      return fetch(`https://api.cryptonator.com/api/full/${c.id}-usd`)
        .then(response => response.json())
        .then(data => {
          const key = `${c.id}USD`;
          this.setState(byPropKey(key, data.ticker.price));
        })
        .catch(err => console.log(err));
      //divide the amount by the current price
      // console.log('in the function')
      //return the new amount
    });
  }

  handleChange = e => {
    this.setState({ amount: e.target.value });
  };

  handleSubmit = id => {
    // e.preventDefault();
    this.setState(byPropKey(id, ''));
    // convertToCrypto(id, 500);
    console.log('random stuff');
    NotificationSystem.newInstance({}, n => (notification = n));
    setTimeout(() => showNotification(), 700);
    //trigger a notication here
  };

  render() {
    // const { handleSubmit } = this;
    return (
      <Container>
        <Row>
          <Col md={12}>
            <h3 className="page-title">Deposit</h3>
            <h3 className="page-subhead subhead">
              Make deposits to your account
            </h3>
          </Col>
        </Row>
        <Row>
          <Col md={18} xs={12}>
            <Row>
              {currencies.map((c, i) => (
                <Col md={4} lg={4} key={i}>
                  <CurrencyInput
                    title={c.title}
                    image={c.image}
                    handleSubmit={e => {
                      e.preventDefault();
                      console.log('random shii');
                    }}
                    handleChange={event =>
                      this.setState(byPropKey(c.id, event.target.value))
                    }
                    amount={this.state[c.id]}
                    amountInCrypto={this.state[c.id] / this.state[`${c.id}USD`]}
                    address={c.address}
                  />
                </Col>
              ))}
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withTranslation('common')(Deposit);
