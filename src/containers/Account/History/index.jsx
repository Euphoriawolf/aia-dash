import React, { PureComponent } from 'react';
import { Container } from 'reactstrap';
import BasicTable from '../../Tables/BasicTables/components/BasicTable';
import { withTranslation } from 'react-i18next';

const sizes = {
  md: 12,
  lg: 12,
  xl: 12
};

class History extends PureComponent {
  render() {
    return (
      <Container>
        <BasicTable sizes={sizes} />
      </Container>
    );
  }
}

export default withTranslation('common')(History);
