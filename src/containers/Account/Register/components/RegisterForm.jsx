import React, { PureComponent } from 'react';
import KeyVariantIcon from 'mdi-react/KeyVariantIcon';
import AccountOutlineIcon from 'mdi-react/AccountOutlineIcon';
import MailRuIcon from 'mdi-react/MailRuIcon';
import { withRouter } from 'react-router-dom';
import { auth, db } from '../../../../utils/firebase';

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value
});

const initialState = {
  username: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  error: null,
  showPassword: false
};

class RegisterForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { ...initialState };

    this.showPassword = this.showPassword.bind(this);
  }

  onSubmit = event => {
    const { username, email, passwordOne } = this.state;
    const { history } = this.props;
    auth
      .doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        //add user to local storage
        const id = authUser.user.uid;
        localStorage.setItem('uid', id);
        localStorage.setItem('email', email);

        //create user in db
        db.doCreateUser(id, username, email)
          .then(() => {
            this.setState({ ...initialState });
            history.push('/home');
          })
          .catch(error => {
            this.setState(byPropKey('error', error));
          });
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });
    event.preventDefault();
  };

  showPassword(e) {
    e.preventDefault();
    this.setState(prevState => ({ showPassword: !prevState.showPassword }));
  }

  render() {
    const { showPassword } = this.state;
    const { username, email, passwordOne } = this.state;

    return (
      <form className="form" onSubmit={this.onSubmit}>
        <div className="form__form-group">
          <span className="form__form-group-label">Full Name</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <AccountOutlineIcon />
            </div>
            <input
              type="text"
              value={username}
              placeholder="e.g John Doe"
              onChange={event =>
                this.setState(byPropKey('username', event.target.value))
              }
            />
          </div>
        </div>
        <div className="form__form-group">
          <span className="form__form-group-label">E-mail</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <MailRuIcon />
            </div>
            <input
              type="email"
              value={email}
              placeholder="e.g johndoe@gmail.com"
              onChange={event =>
                this.setState(byPropKey('email', event.target.value))
              }
            />
          </div>
        </div>
        <div className="form__form-group form__form-group--forgot">
          <span className="form__form-group-label">Password</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <KeyVariantIcon />
            </div>
            <input
              type="password"
              value={passwordOne}
              placeholder="at least 6 characters"
              onChange={event =>
                this.setState(byPropKey('passwordOne', event.target.value))
              }
            />

            <button
              type="button"
              className={`form__form-group-button${
                showPassword ? ' active' : ''
              }`}
              onClick={e => this.showPassword(e)}
            >
            </button>
          </div>
        </div>
        <div className="account__btns">
          <button className="btn btn-primary account__btn">Sign Up</button>
        </div>
      </form>
    );
  }
}

export default withRouter(RegisterForm);
