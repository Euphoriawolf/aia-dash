import React, { PureComponent } from 'react';
import {
  Col,
  Container,
  Row,
  Card,
  CardBody,
  Button,
  ButtonToolbar
} from 'reactstrap';
import { withTranslation } from 'react-i18next';
import { BasicNotification } from '../../../shared/components/Notification';
import NotificationSystem from 'rc-notification';
import ProfileMain from '../Profile/components/ProfileMain';
import * as emailjs from 'emailjs-com';

let notification = null;

const showNotification = () => {
  notification.notice({
    content: (
      <BasicNotification
        title="👋 withdrawal Request received"
        message="A request ticket has been sent and you'll be contacted via support"
      />
    ),
    duration: 5,
    closable: true,
    style: { top: 0, left: 'calc(100vw - 100%)' },
    className: 'right-up'
  });
};

const profileSizes = {
  md: 4,
  lg: 4,
  xl: 4,
  xs: 12
};

class Withdrawal extends PureComponent {
  state = {
    amount: ''
  };

  handleChange = e => {
    this.setState({ amount: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ amount: '' });
    NotificationSystem.newInstance({}, n => (notification = n));
    setTimeout(() => showNotification(), 700);
    //trigger a notication here
    const receiver = localStorage.getItem('email');
    const { amount } = this.state;
    emailjs
      .send(
        'sendgrid',
        'send_withdrawal',
        { amount, receiver },
        'user_BCrP3aaHARFs5PuEbnugg'
      )
      .then(msg => console.log(msg))
      .catch(err => console.log(err));

    // TODO: send an email to support here
  };

  render() {
    const { handleSubmit, handleChange } = this;
    const { amount } = this.state;
    return (
      <Container>
        <Row>
          <Col md={12}>
            <h3 className="page-title">withdrawal</h3>
            <h3 className="page-subhead subhead">
              Make withdrawals to your account
            </h3>
          </Col>
        </Row>
        <Row>
          <Col md={8} lg={8}>
            <Card>
              <CardBody>
                <form className="form form--horizontal" onSubmit={handleSubmit}>
                  <span className="form__form-group-label">Amount($)</span>

                  <div className="form__form-group-field">
                    <input
                      value={amount}
                      type="number"
                      placeholder="enter amount"
                      onChange={handleChange}
                    />
                  </div>
                  <ButtonToolbar className="form__button-toolbar">
                    <Button color="primary" type="submit">
                      Submit
                    </Button>
                    <Button
                      type="button"
                      onClick={() => this.setState({ amount: '' })}
                    >
                      Cancel
                    </Button>
                  </ButtonToolbar>
                </form>
              </CardBody>
            </Card>
          </Col>
          <ProfileMain sizes={profileSizes} />
        </Row>
      </Container>
    );
  }
}

export default withTranslation('common')(Withdrawal);
