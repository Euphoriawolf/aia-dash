import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const config = {
  apiKey: 'AIzaSyA3TBUjBF9fcPgrIxKYN-X-S3oAa_8brWc',
  authDomain: 'aia-dash.firebaseapp.com',
  databaseURL: 'https://aia-dash.firebaseio.com',
  projectId: 'aia-dash',
  storageBucket: 'aia-dash.appspot.com',
  messagingSenderId: '302673715368'
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.firestore();
const auth = firebase.auth();

export { auth, db };
