import { db } from './firebase';
import { generateId } from '../referral';
import * as emailjs from 'emailjs-com';

// import { getID } from './auth';
// import { getCurrentUser } from './auth';

//get balance, transaction history, profit, last deposit and withdrawal

//create withdrawal and deposit

//confirm referral code

const initialAccount = {
  balance: '0',
  profit: '0',
  lastDeposit: '0',
  lastWithdrawal: '0',
  commissions: '0'
};

export const doCreateUser = (id, username, email) => {
  const rCode = generateId();
  return db
    .collection('Users')
    .doc(`${id}`)
    .set({
      username,
      email,
      rCode,
      account: initialAccount,
      transactions: [],
      avatarURL: 'https://ui-avatars.com/api/?background=0D8ABC&color=fff',
      country: '',
      postalCode: '',
      profession: '',
      phoneNumber: '',
      documentURL: 'https://www.svgrepo.com/show/166493/document.svg'
    })
    .then(function() {
      console.log('Document successfully written!');
      emailjs
        .send(
          'sendgrid',
          'send_welcome',
          { name: username, receiver: email },
          'user_ZpHWcPyQ5dhJXRM9c0U2k'
        )
        .then(msg => console.log(msg))
        .catch(err => console.log(err));
    })
    .catch(function(error) {
      console.error('Error writing document: ', error);
    });
};

export const getRCode = code => {
  const userRef = db.collection('Users');

  const query = userRef
    .where('rCode', '==', code)
    .get()
    .then(snapshot => {
      snapshot.docs.forEach(doc => {
        if (doc.exists) {
          console.log('Document data:', doc.data());
        } else {
          // doc.data() will be undefined in this case
          console.log('No such document!');
        }
      });
    })
    .catch(function(error) {
      console.log('Error getting document:', error);
    });

  return query;
};

export const getTransactions = () => {
  const id = localStorage.getItem('uid');
  const userRef = db.collection('Users').doc(`${id}`);

  const t = userRef.get().then(doc => {
    if (doc.exists) {
      // console.log(doc.data().transactions);
      return doc.data().transactions;
    } else {
      console.log('Transactions not found');
    }
  });
  // console.log(t);
  return t;
};

export const onceGetUsers = () => db.ref('users').once('value');

// other entities api
