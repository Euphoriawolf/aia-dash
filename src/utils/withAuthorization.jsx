/* eslint-disable react/prop-types */
import React from 'react';
import { withRouter } from 'react-router-dom';
import { firebase } from './firebase';
import AuthUserContext from './AuthUserContext';

const withAuthorization = authCondition => Component => {
  class withAuthorization extends React.Component {
    componentDidMount() {
      firebase.auth.onAuthStateChanged(authUser => {
        const { history } = this.props;
        if (!authCondition(authUser)) {
          history.push('/log_in');
        }
      });
    }

    render() {
      return (
        <AuthUserContext.Consumer>
          {authUser => (authUser ? <Component /> : null)}
        </AuthUserContext.Consumer>
      );
    }
  }
  return withRouter(withAuthorization);
};

export default withAuthorization;
